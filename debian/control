Source: argparse4j
Section: java
Priority: optional
Maintainer: Debian Java maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hp.com>
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               javahelper,
               junit4,
               libmaven-javadoc-plugin-java,
               libmaven-source-plugin-java,
               maven-debian-helper
Standards-Version: 4.5.1
Homepage: https://argparse4j.github.io/
Vcs-Git: https://salsa.debian.org/java-team/argparse4j.git
Vcs-Browser: https://salsa.debian.org/java-team/argparse4j

Package: libargparse4j-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: libargparse4j-java-doc, ${maven:OptionalDepends}
Description: command-line argument parser for Java based on Python's argparse module
 Argparse4j is a command line argument parser library for Java based on
 Python's argparse module.  The features of argparse4j include:
 .
   * Supported positional, optional and variable number of arguments.
   * Generates well formatted line-wrapped help message.
   * Suggests optional arguments/sub-command if unrecognized
     arguments/sub-command were given.
   * Takes into account East Asian Width ambiguous characters when line-wrap.
 .
 This package contains the library files for libarparse4j-java.

Package: libargparse4j-java-doc
Architecture: all
Section: doc
Depends: ${maven:DocDepends}, ${misc:Depends}
Suggests: libargparse4j-java, ${maven:DocOptionalDepends}
Description: documentation for libargparse4j-java
 Argparse4j is a command line argument parser library for Java based on
 Python's argparse module.  The features of argparse4j include:
 .
   * Supported positional, optional and variable number of arguments.
   * Generates well formatted line-wrapped help message.
   * Suggests optional arguments/sub-command if unrecognized
     arguments/sub-command were given.
   * Takes into account East Asian Width ambiguous characters when line-wrap.
 .
 This package contains the API documentation of libargparse4j-java.
